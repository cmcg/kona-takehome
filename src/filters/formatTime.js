import moment from "moment";

export function formatTime(date) {
    // Convert date string to number in order to produce human readable date/time
    // NOTE: this is a simple front-end facing mask, the timestamp in the DB can
    // still be referenced directly as the raw date is not modified
    const convertDecimal = parseInt(date.split(".")[0]);
    let momentTimestamp = moment
        .unix(convertDecimal)
        .format("MMM D YYYY, h:mm a");
    return momentTimestamp;
}