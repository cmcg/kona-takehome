export function formatSelection(value) {
    switch (value) {
        case "green":
            return value.replace(value, "🟢");
        case "yellow":
            return value.replace(value, "🟡");
        case "red":
            return value.replace(value, "🔴");
        default:
            return value;
    }
}