export function formatEmotion(value) {
    switch (value) {
        case "yeehaw":
            return value.replace(value, "🤠");
        case "fatigued":
            return value.replace(value, "😫");
        case "tired":
            return value.replace(value, "😫");
        case "energized":
            return value.replace(value, "⚡");
        case "anxious":
            return value.replace(value, "😰");
        case "money_in_the_bank!":
            return value.replace(value, "🤑");
        case "refreshed":
            return value.replace(value, "😌");
        case "celebratory":
            return value.replace(value, "🥳");
        case "punk_isn’t_dead":
            return value.replace(value, "🤘");
        case "new_york_city_:')":
            return value.replace(value, "🗽");
        default:
            return value;
    }
}